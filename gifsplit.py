#!/usr/bin/env python

##
## Copyright 2014 Slimy-Beast.com
## Licensed under The MIT License (MIT)
##
## A copy of the license can be found
## on the projects repository.
## https://bitbucket.org/slimybeast/gifsplit
##


import os
import glob
import urllib
import shutil
import argparse
import subprocess

from decimal import Decimal


def test_gif(file_name):
    """
        Ensure the file downloaded was in fact a gif.
    """
    
    if file_name.split('.')[1] != 'gif':
        raise Exception('Image is not a gif.')
    
def test_size(file_name):
    """
        Ensure the gif file downloaded was over 1MB.
    """
    
    if os.path.getsize(file_name) <= 1048576:
        raise Exception('File is already 1MB or smaller.')
    
def count_gifs(file_name):
    """
        Get the count of new gifs that will need to be created.
    """
    
    new_images = int(round(os.path.getsize(file_name) / Decimal(1048576)))
    return new_images
    
def extract_frames(file_name, frames_dir):
    """
        Use ImageMagick to extract frames from the original gif.
    """
    
    subprocess.call(['convert', '-coalesce', image_file, frames_dir + '/%04d.gif'])
    
def build_gif(frames_dir, frames_start, frames_end, output_dir, output_cnt):
    """
        Build a new gif based off the frames send to the function.
    """
    
    process = 'convert +dither -colors 128 -layers Optimize -loop 0 ' +\
              frames_dir + '/%04d.gif[' + str(frames_start) + '-' + str(frames_end) + '] ' +\
              output_dir + '/output' + str(output_cnt) + '.gif'

    p = subprocess.Popen(process, shell=True)
    
    (output, err) = p.communicate()
    p_status = p.wait()
    
def optimize_gifs(gif_file):
    """
        Use gifsicle to optimize the newly created files.
    """
    
    subprocess.call(['gifsicle', '-b', '-O3', gif_file])
    
def cleanup(image_file, frames_dir):
    """
        Cleanup files no longer needed.
    """
    
    shutil.rmtree(frames_dir)
    os.remove(image_file)


if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument("image_url", help="URL to the gif file to split.",
                        type=str)
    args = parser.parse_args()
    
    # Base vars
    image_url = args.image_url
    image_file = image_url.split('/')[-1]
    frames_dir = 'frames'
    output_dir = 'output'
    frame_cnt = 0
    output_cnt = 0
    
    # Check to ensure the directories exist.
    if os.path.isdir('frames') != True:
        os.mkdir('frames')
    if os.path.isdir('output') != True:
        os.mkdir('output')
    
    test_gif(image_file)
    
    urllib.urlretrieve(image_url, image_file)
    
    test_size(image_file)
    
    extract_frames(image_file, frames_dir)
    
    num_gifs = count_gifs(image_file)
    
    extracted_frames = glob.glob(frames_dir + '/*.gif')
    
    frames_per_gif = len(extracted_frames) / num_gifs
    frames_per_gif_remainder = len(extracted_frames) % num_gifs
    
    while output_cnt < num_gifs:
    
        if output_cnt == 0:
            frames_start = frame_cnt
        else:
            frames_start = frame_cnt + 1
    
        frames_end = frame_cnt + frames_per_gif
        
        build_gif(frames_dir, frames_start, frames_end, output_dir, output_cnt)
    
        output_cnt += 1
        frame_cnt = frames_end
        
    for f in glob.glob(output_dir + '/*.gif'):
        optimize_gifs(f)
        
    cleanup(image_file, frames_dir)