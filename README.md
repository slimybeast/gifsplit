# Gifsplit.py
---

## About
---

Gifsplit splits a gif image that is over 1MB into smaller pieces.  Originally put together to allow uploading larger gif files to Tumblr.

## Requirements
---

Gifsplit was developed and tested on Linux.  However it may work on Windows or Mac with the following applications installed.

* Python 2.7+
* ImageMagick (http://www.imagemagick.org/)
* Gifsicle (http://www.lcdf.org/gifsicle/)

## Installation
---

With the requirements installed on your local computer, all that is required is to download the gifsplit.py locally.

## Usage
---

There is only a single command line argument for Gifsplit at this time.

``` 
python gifsplit.py $url
```

This will then create, if needed, and populate two directories in the current directory as where gifsplit.py is being called from.  One directory called frames holds the extracted frames from the gif file.  The other is the output directory which stores the files created by gifsplit.py.  Additionally the image being split will be downloaded to this directory.

Once Gifsplit finishes it will then cleanup (remove) the frames directory and the downloaded original file.

### Example

```
python gifsplit.py http://i.imgur.com/UcyJgro.gif
```

## License
---

Please see the LICENSE file included in this repo.
